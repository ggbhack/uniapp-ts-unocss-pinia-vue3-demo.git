import { defineConfig } from 'vite'
import path from 'path'
import viteConfig from './vite'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [viteConfig()],
  server: {
    port: 3100,
    host: '0.0.0.0',
    proxy: {
      '/api': {
        target: 'https://rebot.phpnba.cn/api',
        changeOrigin: true,
        rewrite: (p) => p.replace(/^\/api/, ''),
      },
      '/server_api': {
        target: 'http://vop.baidu.com/server_api',
        changeOrigin: true,
        rewrite: (p) => p.replace(/^\/server_api/, ''),
      },
    },
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
      '/@': path.resolve(__dirname, './src'),
      '/#': path.resolve(__dirname, './types'),
      '@components': path.resolve(__dirname, './src/components'),
    },
  },
})
