export interface UserInfo {
  userId?: string | number;
  username?: string;
  realName?: string;
  avatar?: string;
  desc?: string;
  homePath?: string;
  // 创建时间
  create_time: string;
  // 是否删除
  is_delete: boolean;
  // 是否是总门店
  is_super: boolean;
  // 密码
  password: string;
  // 名字
  real_name: string;
  // 排序
  sort: number;
  // 门店id
  store_id: number;
  // 门店用户id
  store_user_id: number;
  // 更新时间
  update_time: string;
  // 用户名
  user_name: string;
}
