// generated by unplugin-vue-components
// We suggest you to commit this file into source control
// Read more: https://github.com/vuejs/vue-next/pull/3399

declare module 'vue' {
  export interface GlobalComponents {
    ToggleAppTheme: (theme: string) => void
  }
  interface ComponentCustomProperties {
    AppTheme: string
    ToggleAppTheme: (theme: string) => void
  }
}

export {}
