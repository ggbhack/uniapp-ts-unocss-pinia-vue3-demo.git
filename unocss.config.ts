import presetWeapp from 'unocss-preset-weapp'
import { defineConfig } from 'unocss'
import {
  transformerAttributify,
  transformerClass,
} from 'unocss-preset-weapp/transformer'

export default defineConfig({
  presets: [
    // https://github.com/MellowCo/unocss-preset-weapp
    presetWeapp(),
  ],
  shortcuts: [],
  rules: [
    [/^m-([\.\d]+)$/, ([_, num]) => ({ margin: `${num}rpx` })],
    [/^m-t-([\.\d]+)$/, ([_, num]) => ({ 'margin-top': `${num}rpx` })],
    [/^m-b-([\.\d]+)$/, ([_, num]) => ({ 'margin-bottom': `${num}rpx` })],
    [/^m-l-([\.\d]+)$/, ([_, num]) => ({ 'margin-left': `${num}rpx` })],
    [/^m-r-([\.\d]+)$/, ([_, num]) => ({ 'margin-right': `${num}rpx` })],
    [
      /^m-x-([\.\d]+)$/,
      ([_, num]) => ({
        'margin-left': `${num}rpx`,
        'margin-right': `${num}rpx`,
      }),
    ],
    [
      /^m-y-([\.\d]+)$/,
      ([_, num]) => ({
        'margin-top': `${num}rpx`,
        'margin-bottom': `${num}rpx`,
      }),
    ],
    [/^p-([\.\d]+)$/, ([_, num]) => ({ padding: `${num}rpx` })],
    [/^p-t-([\.\d]+)$/, ([_, num]) => ({ 'padding-top': `${num}rpx` })],
    [/^p-b-([\.\d]+)$/, ([_, num]) => ({ 'padding-bottom': `${num}rpx` })],
    [/^p-l-([\.\d]+)$/, ([_, num]) => ({ 'padding-left': `${num}rpx` })],
    [/^p-r-([\.\d]+)$/, ([_, num]) => ({ 'padding-right': `${num}rpx` })],
    [
      /^p-x-([\.\d]+)$/,
      ([_, num]) => ({
        'padding-left': `${num}rpx`,
        'padding-right': `${num}rpx`,
      }),
    ],
    [
      /^p-y-([\.\d]+)$/,
      ([_, num]) => ({
        'padding-top': `${num}rpx`,
        'padding-bottom': `${num}rpx`,
      }),
    ],
    [
      /^f-([\.\d]+)$/,
      ([_, num]) => ({
        'font-size': `${num}rpx`,
      }),
    ],
    [
      /^vh-([\.\d]+)$/,
      ([_, num]) => ({
        height: `${num}vh`,
      }),
    ],
    [
      /^vw-([\.\d]+)$/,
      ([_, num]) => ({
        width: `${num}vw`,
      }),
    ],
  ],
  transformers: [
    // https://github.com/MellowCo/unocss-preset-weapp/tree/main/src/transformer/transformerAttributify
    transformerAttributify() as any,
    // https://github.com/MellowCo/unocss-preset-weapp/tree/main/src/transformer/transformerClass
    transformerClass(),
  ],
})
