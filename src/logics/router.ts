export const routerConfig = {
  /*
    名词解释：“强制登录页”
        在打开定义的需强制登录的页面之前会自动检查（前端校验）uni_id_token的值是否有效,
        如果无效会自动跳转到登录页面
    两种模式：
        1.needLogin：黑名单模式。枚举游客不可访问的页面。
        2.visitor：白名单模式。枚举游客可访问的页面。
    * 注意：黑名单与白名单模式二选一
        */
  // "needLogin" : [
  //  	{pattern:/^\/pages\/list.*/},	//支持正则表达式
  // 		"/uni_modules/uni-news-favorite/pages/uni-news-favorite/list",
  // 		"/uni_modules/uni-feedback/pages/uni-feedback/add"
  // ],
  tabbars: [],
  visitor: [
    '/', // 注意入口页必须直接写 "/"
    '/pages/login/login',
    // '/pages/index/index',
    // '/pages/bindDevice/bindDevice',
    // '/pages/logs/logs'
  ],
}
