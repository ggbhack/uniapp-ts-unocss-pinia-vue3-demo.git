/**
 * Application configuration
 */
// import type { ProjectConfig } from '/#/config';

// import projectSetting from '/@/settings/projectSetting'
import { PageEnum } from '@/enums/pageEnum'
import { useAppStore } from '/@/store/modules/app'
import { useLocaleStore } from '/@/store/modules/locale'
import { routerConfig } from './router'
import { getToken } from '@/utils/auth'
// import { useUserStore } from '@/store/modules/user'

const Debug = false

// Initial project configuration
export async function initAppConfigStore() {
  console.log('initAppConfigStore')
  const localeStore = useLocaleStore()
  const appStore = useAppStore()
  // const userStore = useUserStore()
  // await userStore.userLogin()
  // userStore.getUserInfoAction()
  // init store
  localeStore.initLocale()
  // 初始化基础信息
  appStore.initApp()
}

// Initial project
export async function initApp() {
  console.log('initApp')

  // 主动更新小程序
  // #ifndef H5
  updateManager()
  // #endif
  // 路由拦截
  // routerDefine()
}

/**
 * 路由拦截
 */
export function routerDefine() {
  // 自定义路由拦截
  const { visitor } = routerConfig // 需要登录的页面
  // 路由拦击
  const list = ['navigateTo', 'redirectTo', 'reLaunch', 'switchTab']
  list.forEach((item) => {
    // 用遍历的方式分别为,uni.navigateTo,uni.redirectTo,uni.reLaunch,uni.switchTab这4个路由方法添加拦截器
    uni.addInterceptor(item, {
      invoke(e) {
        console.log('进入拦截', e)
        // 调用前拦截
        // 获取用户的token
        const token = getToken()
        // 获取要跳转的页面路径（url去掉"?"和"?"后的参数）
        const url = e.url.split('?')[0]
        // 获取要前往的页面路径（即url去掉"?"和"?"后的参数）
        const pages = getCurrentPages()
        if (!pages.length) {
          console.log('首页启动调用了')
          return e
        }
        const fromUrl = pages[pages.length - 1].route
        const inLoginPage = fromUrl?.split('/')[1] === 'login'

        // 控制登录优先级
        if (
          // 判断当前窗口是否为登录页面，如果是则不重定向路由
          url === PageEnum.BASE_LOGIN &&
          !inLoginPage
        ) {
          // // 一键登录（univerify）、账号（username）、验证码登录（短信smsCode）
          // if (login[0] === 'username') {
          //   e.url = PageEnum.BASE_LOGIN
          // } else {
          //   if (e.url === url) {
          //     e.url += '?'
          //   } // 添加参数之前判断是否带了`？`号如果没有就补上，因为当开发场景本身有参数的情况下是已经带了`？`号
          //   e.url += `type=${login[0]}`
          // }
        } else {
          // 拦截强制登录页面
          let pass = true
          // pattern
          // if (needLogin) {
          // 	pass = needLogin.every((item) => {
          // 		if (typeof (item) == 'object' && item.pattern) {
          // 			return !item.pattern.test(url)
          // 		}
          // 		return url != item
          // 	})
          // 	// console.log({pass})
          // }
          if (visitor) {
            // eslint-disable-next-line no-shadow
            pass = visitor.some((item: any) => {
              if (typeof item === 'object' && item.pattern) {
                return item.pattern.test(url)
              }
              return url === item
            })
            // console.log({pass})
          }
          // 防止多次跳转login页面
          if (url === PageEnum.BASE_LOGIN && inLoginPage) {
            return false
          }

          if (!pass && !token) {
            uni.showToast({
              title: '请先登录',
              icon: 'none',
            })

            uni.navigateTo({
              url: PageEnum.BASE_LOGIN,
              type: 'redirectTo',
            })
            return false
          }
        }
        return e
      },
      fail(err) {
        // 失败回调拦截
        console.log('拦截失败', err)
        if (Debug) {
          console.log(err)
          uni.showModal({
            content: JSON.stringify(err),
            showCancel: false,
          })
        }
      },
    })
  })
}

/**
 * 小程序主动更新
 */
// 更新小程序
function updateManager() {
  // 本API返回全局唯一的版本更新管理器对象： updateManager，用于管理小程序更新；App的更新不使用本API
  const updateManagerMini = uni.getUpdateManager()
  updateManagerMini.onCheckForUpdate(() => {
    // 请求完新版本信息的回调
    // console.log(res.hasUpdate)
  })
  // onUpdateReady 当新版本下载完成，会进行回调
  updateManagerMini.onUpdateReady(() => {
    uni.showModal({
      title: '更新提示',
      content: '新版本已经准备好，即将重启应用',
      showCancel: false,
      success(res) {
        if (res.confirm) {
          // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
          updateManagerMini.applyUpdate()
        }
      },
    })
  })
  updateManagerMini.onUpdateFailed(() => {
    // 新的版本下载失败
    uni.showModal({
      title: '更新提示',
      content: '新版本下载失败',
      showCancel: false,
    })
  })
}
