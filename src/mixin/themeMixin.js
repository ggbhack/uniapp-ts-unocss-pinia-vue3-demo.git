import { useUserStoreWithOut } from '/@/store/modules/app'
import { hidePageNavInWechatBrowser } from '/@/utils/uniUtil'

/**
 * 主题的Mixin
 */
export default {
  onReady() {
    const appStore = useUserStoreWithOut()
    appStore.changeSysTheme()
    hidePageNavInWechatBrowser()
  },
}
