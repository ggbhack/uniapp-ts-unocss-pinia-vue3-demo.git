import { ThemeEnum } from '@/enums/themeEnum'
import { themeConfig } from '@/config/theme'
import { useUserStoreWithOut } from '/@/store/modules/app'
/**
 * 主题配置
 * @returns
 */
export const useThemeConfig = () => {
  const appStore = useUserStoreWithOut()
  const primaryBtnTheme = () => {
    const theme = appStore.theme || ThemeEnum.THEME_DEFAULT
    const current = themeConfig[theme]
    return {
      backgroundColor: current.primary,
      border: 'none',
    }
  }
  return {
    primaryBtnTheme,
  }
}
