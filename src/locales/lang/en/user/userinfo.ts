/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2022-04-21 21:12:54
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-30 11:48:49
 */
export default {
  navigationBarTitle: 'My Profile',
  ProfilePhoto: 'Profile Photo',
  nickname: 'Nickname',
  notSet: 'not set',
  phoneNumber: 'Phone Number',
  notSpecified: 'Not Specified',
  setNickname: 'Set Nickname ',
  setNicknamePlaceholder: 'Please enter a nickname to set',
  bindPhoneNumber: 'One click binding of local number',
  bindOtherLogin: 'Other number binding',
  noChange: 'No change',
  uploading: 'uploading',
  requestFail: 'Request for service failed',
  setting: 'setting',
  deleteSucceeded: 'Delete succeeded',
  setSucceeded: 'Set successfully',
  bindInfo: 'bindInfo',
  getPhoneAndBind: 'getPhoneAndBind',
  bindSucceeded: 'bindSucceeded',
  newsLog: {
    navigationBarTitle: 'Reading Log',
  },
  bindMobile: {
    navigationBarTitle: 'Bind Mobile',
  },
  deactivate: {
    cancelText: 'cancel',
    nextStep: 'next step',
    navigationBarTitle: 'Logout prompt',
    succeeded: 'succeeded',
    content:
      'You have carefully read the logout prompt, know the possible consequences, and confirm that you want to logout',
  },
}
