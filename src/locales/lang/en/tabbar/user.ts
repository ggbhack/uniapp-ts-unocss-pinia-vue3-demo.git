/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2022-04-21 21:10:15
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-21 21:30:27
 */
export default {
  showText: 'Text',
  signIn: 'Check In Reward',
  signInByAd: 'Check In Reward By AD',
  toEvaluate: 'To Evaluate',
  readArticles: 'Read Articles',
  myScore: 'My Score',
  invite: 'Invite Friends',
  feedback: 'Problems And Feedback',
  settings: 'Settings',
  about: 'About',
  checkUpdate: 'Check for Updates',
  clicked: 'You Clicked',
  checkScore: 'Please check your points after logging in',
  currentScore: 'The current score is ',
  noScore: 'There are currently no points',
  notLogged: 'not logged in',
}
