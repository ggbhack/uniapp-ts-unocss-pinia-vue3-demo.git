/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2022-04-30 07:04:27
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-30 07:06:56
 */
/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2022-04-30 06:50:30
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-30 07:03:25
 */
export default {
  agreement: 'Please agree to the relevant agreement first',
  notice: '凡客行 Apply for the following permissions:',
  tips: 'Get your public information (nickname, avatar, etc.)',
  profile: 'Used to set account nickname and avatar',
  btn: 'login',
}
