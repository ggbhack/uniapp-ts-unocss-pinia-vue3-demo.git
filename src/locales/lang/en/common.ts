/*
 * @Description: 你的代码我的心
 * @Author: GGB
 * @Date: 2022-04-21 12:16:50
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-21 21:30:03
 */
export default {
  wechatFriends: 'friends',
  wechatBbs: 'bbs',
  weibo: 'weibo',
  more: 'more',
  agree: 'agree',
  copy: 'copy',
  wechatApplet: 'applet',
  cancelShare: 'cancel sharing',
  updateSucceeded: 'update succeeded',
  phonePlaceholder: 'Please enter your mobile phone number',
  verifyCodePlaceholder: 'Please enter the verification code',
  newPasswordPlaceholder: 'Please enter a new password',
  confirmNewPasswordPlaceholder: 'Please confirm the new password',
  confirmPassword: 'Please confirm the password',
  verifyCodeSend: 'Verification code has been sent to via SMS',
  passwordDigits: 'The password is 6 - 20 digits',
  getVerifyCode: 'Get Code',
  noAgree: 'You have not agreed to the privacy policy agreement',
  gotIt: 'got it',
  login: 'sign in',
  error: 'error',
  complete: 'complete',
  submit: 'Submit',
  formatErr: 'Incorrect mobile phone number format',
  sixDigitCode: 'Please enter a 6-digit verification code',
  resetNavTitle: 'Reset password',
  copySuccess: 'Copy successfully',
}
