/*
 * @Description: 你的代码我的心
 * @Author: GGB
 * @Date: 2022-04-21 13:09:59
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-21 21:30:14
 */
export default {
  list: {
    inputPlaceholder: 'Please enter the search content',
  },
  search: {
    cancelText: 'cancel',
    searchHistory: 'search history',
    searchDiscovery: 'search discovery',
    deleteAll: 'delete all',
    delete: 'delete',
    deleteTip: 'Are you sure to clear the search history ?',
    complete: 'complete',
    searchHiddenTip: 'Current search found hidden',
  },
  grid: {
    grid: 'Grid Assembly',
    visibleToAll: 'Visible to all',
    invisibleToTourists: 'Invisible to tourists',
    adminVisible: 'Admin visible',
    clickTip: 'Click the',
    clickTipGrid: 'grid',
  },
}
