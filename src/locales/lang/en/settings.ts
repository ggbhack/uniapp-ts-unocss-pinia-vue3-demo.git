/*
 * @Description: 你的代码我的心
 * @Author: GGB
 * @Date: 2022-04-21 13:10:37
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-21 21:31:23
 */
export default {
  navigationBarTitle: 'Settings',
  userInfo: 'Personal Data',
  changePassword: 'change password',
  clearTmp: 'clean cache',
  pushServer: 'push function',
  fingerPrint: 'fingerprint unlock',
  facial: 'face unlock',
  deactivate: 'Deactivate',
  logOut: 'Logout',
  login: 'Login',
  changeLanguage: 'Language',
  please: 'please',
  successText: 'success',
  failTip: 'Authentication failed. Please try again',
  authFailed: 'authentication failed',
  deviceNoOpen: 'The device is not turned on',
  fail: 'fail',
  tips: 'tips',
  exitLogin: 'Do you want to log out？',
  cancelText: 'cancel',
  confirmText: 'confirm',
  clearing: 'clearing',
  clearedSuccessed: 'Cleared successfully',
}
