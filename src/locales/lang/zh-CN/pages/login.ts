/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2022-04-30 06:50:30
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-30 07:03:25
 */
export default {
  agreement: '请先同意相关协议',
  notice: '凡客行申请获得以下权限：',
  tips: ' 获取你的公共信息（昵称，头像等）',
  profile: '用于设置账户昵称和头像',
  btn: '登录',
}
