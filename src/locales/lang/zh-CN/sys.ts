/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2022-04-21 21:21:51
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-30 11:44:44
 */
export default {
  tabbar: {
    index: '首页',
    health: '健康圈',
    store: '商城',
    user: '我的',
    active: '进行中',
  },
  button: {
    close: '关闭',
    get: '获取',
  },
  message: {
    authSuccess: '授权成功',
    authFail: '授权失败',
    authing: '授权中',
    init: '信息初始化',
  },
  agreementsTitle: '用户服务协议,隐私政策',
  uniQuickLogin: {
    accountLogin: '账号登录',
    SMSLogin: '短信验证码',
    wechatLogin: '微信登录',
    appleLogin: '苹果登录',
    oneClickLogin: '一键登录',
    QQLogin: 'QQ登录',
    xiaomiLogin: '小米登录',
    getProviderFail: '获取服务供应商失败',
    loginErr: '登录服务初始化错误',
    chooseOtherLogin: '点击了第三方登录',
  },
  pwdLogin: {
    pwdLogin: '用户名密码登录',
    placeholder: '请输入手机号/用户名',
    passwordPlaceholder: '请输入密码',
    verifyCodePlaceholder: '请输入验证码',
    login: '登录',
    forgetPassword: '忘记密码',
    register: '注册账号',
  },
  register: {
    navigationBarTitle: '注册',
    usernamePlaceholder: '请输入用户名',
    nicknamePlaceholder: '请输入用户昵称',
    registerAndLogin: '注册并登录',
    passwordDigitsPlaceholder: '请输入6-20位密码',
    passwordAgain: '再次输入密码',
  },

  about: {
    sacnQR: '扫描二维码，您的朋友也可以下载',
    client: '客户端',
    and: '和',
    about: '关于',
  },
  uniFeedback: {
    navigationBarTitle: '问题与反馈',
    msgTitle: '留言内容',
    imgTitle: '图片列表',
    contacts: '联系人',
    phone: '联系电话',
    submit: '提交',
  },
  invite: {
    download: '下载',
  },
  smsCode: {
    resendVerifyCode: '重新发送',
    phoneErrTip: '手机号格式错误',
    sendSuccessTip: '短信验证码发送成功',
  },
  loadMore: {
    noData: '暂无数据',
    noNetwork: '网络异常',
    toSet: '前往设置',
    error: '错误',
  },
  listDetail: {
    follow: '点击关注',
    newsErr: '出错了，新闻ID为空',
  },
  api: {
    operationSuccess: '操作成功',
    operationFailed: '操作失败',
    errorTip: '错误提示',
    successTip: '成功提示',
    errorMessage: '操作失败,系统异常!',
    timeoutMessage: '登录超时,请重新登录!',
    apiTimeoutMessage: '接口请求超时,请刷新页面重试!',
    apiRequestFailed: '请求出错，请稍候重试',
    networkException: '网络异常',
    networkExceptionMsg: '网络异常，请检查您的网络连接是否正常!',

    errMsg401: '用户没有权限（令牌、用户名、密码错误）!',
    errMsg403: '用户得到授权，但是访问是被禁止的。!',
    errMsg404: '网络请求错误,未找到该资源!',
    errMsg405: '网络请求错误,请求方法未允许!',
    errMsg408: '网络请求超时!',
    errMsg500: '服务器错误,请联系管理员!',
    errMsg501: '网络未实现!',
    errMsg502: '网络错误!',
    errMsg503: '服务不可用，服务器暂时过载或维护!',
    errMsg504: '网络超时!',
    errMsg505: 'http版本不支持该请求!',
  },
  app: {
    logoutTip: '温馨提醒',
    logoutMessage: '是否确认退出系统?',
    menuLoading: '菜单加载中...',
  },
  errorLog: {
    tableTitle: '错误日志列表',
    tableColumnType: '类型',
    tableColumnDate: '时间',
    tableColumnFile: '文件',
    tableColumnMsg: '错误信息',
    tableColumnStackMsg: 'stack信息',

    tableActionDesc: '详情',

    modalTitle: '错误详情',

    fireVueError: '点击触发vue错误',
    fireResourceError: '点击触发资源加载错误',
    fireAjaxError: '点击触发ajax错误',

    enableMessage:
      '只在`/src/settings/projectSetting.ts` 内的useErrorHandle=true时生效.',
  },
  exception: {
    backLogin: '返回登录',
    backHome: '返回首页',
    subTitle403: '抱歉，您无权访问此页面。',
    subTitle404: '抱歉，您访问的页面不存在。',
    subTitle500: '抱歉，服务器报告错误。',
    noDataTitle: '当前页无数据',
    networkErrorTitle: '网络错误',
    networkErrorSubTitle: '抱歉，您的网络连接已断开，请检查您的网络！',
  },
  lock: {
    unlock: '点击解锁',
    alert: '锁屏密码错误',
    backToLogin: '返回登录',
    entry: '进入系统',
    placeholder: '请输入锁屏密码或者用户密码',
  },
  login: {
    phoneLogin: '登录后即可展示自己',
    phoneLoginTip: '未注册的手机号验证通过后将自动注册',
    getVerifyCode: '获取验证码',
    success: '登录成功',
    start: '登录中',
    fail: '登录失败',
    update: '更新中...',

    backSignIn: '返回',
    signInFormTitle: '登录',
    mobileSignInFormTitle: '手机登录',
    qrSignInFormTitle: '二维码登录',
    signUpFormTitle: '注册',
    forgetFormTitle: '重置密码',

    signInTitle: '开箱即用的中后台管理系统',
    signInDesc: '输入您的个人详细信息开始使用！',
    policy: '我同意xxx隐私政策',
    scanSign: `扫码后点击"确认"，即可完成登录`,

    loginButton: '登录',
    registerButton: '注册',
    rememberMe: '记住我',
    forgetPassword: '忘记密码?',
    otherSignIn: '其他登录方式',

    // notify
    loginSuccessTitle: '登录成功',
    loginSuccessDesc: '欢迎回来',

    // placeholder
    accountPlaceholder: '请输入账号',
    passwordPlaceholder: '请输入密码',
    smsPlaceholder: '请输入验证码',
    mobilePlaceholder: '请输入手机号码',
    policyPlaceholder: '勾选后才能注册',
    diffPwd: '两次输入密码不一致',

    userName: '账号',
    password: '密码',
    confirmPassword: '确认密码',
    email: '邮箱',
    smsCode: '短信验证码',
    mobile: '手机号码',
  },
  pay: {
    paySuccess: '支付成功',
    payFail: '支付失败',
  },
}
