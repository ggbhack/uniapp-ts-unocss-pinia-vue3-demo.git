/*
 * @Description: 你的代码我的心
 * @Author: GGB
 * @Date: 2022-04-21 13:09:59
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-21 21:27:37
 */
export default {
  list: {
    inputPlaceholder: '请输入搜索内容',
  },
  search: {
    cancelText: '取消',
    searchHistory: '搜索历史',
    searchDiscovery: '搜索发现',
    deleteAll: '全部删除',
    delete: '删除',
    deleteTip: '确认清空搜索历史吗？',
    complete: '完成',
    searchHiddenTip: '当前搜索发现已隐藏',
  },
  grid: {
    grid: '宫格组件',
    visibleToAll: '所有人可见',
    invisibleToTourists: '游客不可见',
    adminVisible: '管理员可见',
    clickTip: '点击第',
    clickTipGrid: '个宫格',
  },
}
