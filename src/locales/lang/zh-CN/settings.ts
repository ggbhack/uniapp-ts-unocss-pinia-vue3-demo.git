/*
 * @Description: 你的代码我的心
 * @Author: GGB
 * @Date: 2022-04-21 13:10:37
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-21 21:14:48
 */
export default {
  navigationBarTitle: '设置',
  userInfo: '个人资料',
  changePassword: '修改密码',
  clearTmp: '清理缓存',
  pushServer: '推送功能',
  fingerPrint: '指纹解锁',
  facial: '人脸解锁',
  deactivate: '注销账号',
  logOut: '退出登录',
  login: '登录',
  failTip: '认证失败请重试',
  authFailed: '认证失败',
  changeLanguage: '切换语言',
  please: '请用',
  successText: '成功',
  deviceNoOpen: '设备未开启',
  fail: '失败',
  tips: '提示',
  exitLogin: '是否退出登录?',
  clearing: '清除中',
  clearedSuccessed: '清除成功',
  confirmText: '确定',
  cancelText: '取消',
}
