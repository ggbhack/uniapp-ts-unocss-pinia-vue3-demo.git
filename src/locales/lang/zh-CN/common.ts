/*
 * @Description: 你的代码我的心
 * @Author: GGB
 * @Date: 2022-04-21 12:16:50
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-21 21:16:19
 */
export default {
  wechatFriends: '微信好友',
  wechatBbs: '微信朋友圈',
  weibo: '微博',
  more: '更多',
  agree: '同意',
  copy: '复制',
  wechatApplet: '微信小程序',
  cancelShare: '取消分享',
  updateSucceeded: '更新成功',
  phonePlaceholder: '请输入手机号',
  verifyCodePlaceholder: '请输入验证码',
  newPasswordPlaceholder: '请输入新密码',
  confirmNewPasswordPlaceholder: '请确认新密码',
  confirmPassword: '请确认密码',
  verifyCodeSend: '验证码已通过短信发送至',
  passwordDigits: '密码为6 - 20位',
  getVerifyCode: '获取验证码',
  noAgree: '你未同意隐私政策协议',
  gotIt: '知道了',
  login: '登录',
  error: '错误',
  complete: '完成',
  submit: '提交',
  formatErr: '手机号码格式不正确',
  sixDigitCode: '请输入6位验证码',
  resetNavTitle: '重置密码',
  download: '下载',
  copySuccess: '复制成功',
}
