/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2022-04-21 21:10:15
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-21 21:11:15
 */
export default {
  showText: '文字',
  signIn: '普通签到',
  signInByAd: '看广告签到',
  toEvaluate: '去评分',
  readArticles: '阅读过的文章',
  myScore: '我的积分',
  invite: '分销推荐',
  feedback: '问题与反馈',
  settings: '设置',
  checkUpdate: '检查更新',
  about: '关于',
  clicked: '你点击了',
  checkScore: '请登录后查看积分',
  currentScore: '当前积分为',
  noScore: '当前无积分',
  notLogged: '未登录',
}
