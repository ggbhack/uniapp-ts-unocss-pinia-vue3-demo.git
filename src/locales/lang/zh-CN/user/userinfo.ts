/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2022-04-21 21:12:54
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-30 11:48:30
 */
export default {
  navigationBarTitle: '个人资料',
  ProfilePhoto: '头像',
  nickname: '昵称',
  notSet: '未设置',
  phoneNumber: '手机号',
  notSpecified: '未绑定',
  setNickname: '设置昵称',
  setNicknamePlaceholder: '请输入要设置的昵称',
  bindPhoneNumber: '本机号码一键绑定',
  bindOtherLogin: '其他号码绑定',
  noChange: '没有变化',
  uploading: '正在上传',
  requestFail: '请求服务失败',
  setting: '设置中',
  deleteSucceeded: '删除成功',
  setSucceeded: '设置成功',
  bindInfo: '绑定资料',
  getPhoneAndBind: '将一键获取你的手机号码绑定你的个人资料',
  bindSucceeded: '绑定成功',
  newsLog: {
    navigationBarTitle: '阅读记录',
  },
  bindMobile: {
    navigationBarTitle: '绑定手机号码',
  },
  deactivate: {
    cancelText: '取消',
    nextStep: '下一步',
    navigationBarTitle: '注销提示',
    succeeded: '注销成功',
    content: '已经仔细阅读注销提示，知晓可能带来的后果，并确认要注销',
  },
}
