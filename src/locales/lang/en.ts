/*
 * @Description: 你的代码我的心
 * @Author: GGB
 * @Date: 2022-04-21 12:16:50
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-21 13:12:37
 */
import { genMessage } from '../helper'
// import momentLocale from 'moment/dist/locale/en-us';

const modules = import.meta.glob('./en/**/*.ts', { eager: true }) as any
export default {
  message: {
    ...genMessage(modules, 'en'),
  },
}
