/*
 * @Description: 你的代码我的心
 * @Author: GGB
 * @Date: 2022-04-21 12:16:50
 * @LastEditors: GGB
 * @LastEditTime: 2022-04-21 13:12:20
 */
import { genMessage } from '../helper'

const modules = import.meta.glob('./zh-CN/**/*.ts', { eager: true }) as any
export default {
  message: {
    ...genMessage(modules, 'zh-CN'),
  },
}
