import { jsonp } from 'vue-jsonp'
/* 地址解析 */
const reverseGeocoderUrl = 'https://apis.map.qq.com/ws/geocoder/v1/'
/* 骑行地址规划 */
const directionUrl = 'https://apis.map.qq.com/ws/direction/v1/bicycling'

/* 骑行地址规划 */
const searchUrl = 'https://apis.map.qq.com/ws/place/v1/search'

export const mapKey = 'ZD3BZ-3Z4CK-AKTJZ-AXMO2-VGEZ2-HTB2G'
export const reverseGeocoder = ({ latitude, longitude }: any) =>
  jsonp(reverseGeocoderUrl, {
    key: mapKey,
    location: `${latitude},${longitude}`,
    output: 'jsonp',
  })
/**
 * mode//可选值：'driving'（驾车）、'walking'（步行）、'bicycling'（骑行），不填默认：'driving',可不填
 */
export const direction = ({ mode, from, to }: any) =>
  jsonp(directionUrl, {
    mode,
    //from参数不填默认当前地址
    from: `${from.latitude},${from.longitude}`,
    to: `${to.latitude},${to.longitude}`,
    key: mapKey,
    output: 'jsonp',
  })

/**
 * mode//可选值：'driving'（驾车）、'walking'（步行）、'bicycling'（骑行），不填默认：'driving',可不填
 */
export const search = ({ keyword, page_size, nearby }: any) =>
  jsonp(searchUrl, {
    keyword,
    page_size,
    boundary: nearby,
    key: mapKey,
    output: 'jsonp',
  })
