import { getStorageShortName } from '/@/utils/env'
import { createStorage as create, CreateStorageParams } from './storageCache'
import {
  DEFAULT_CACHE_TIME,
  enableStorageEncryption,
} from '/@/settings/encryptionSetting'

export type Options = Partial<CreateStorageParams>

const createOptions = (options: Options = {}): Options => {
  return {
    // No encryption in debug mode
    hasEncrypt: enableStorageEncryption,
    prefixKey: getStorageShortName(),
    ...options,
  }
}

export const UniStorage = create(createOptions())

export const createStorage = (options: Options = {}) => {
  return create(createOptions(options))
}

export const createLocalStorage = (options: Options = {}) => {
  return createStorage({ ...options, timeout: DEFAULT_CACHE_TIME })
}

export default UniStorage
