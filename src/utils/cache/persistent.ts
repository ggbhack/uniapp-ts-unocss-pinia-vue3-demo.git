import type { UserInfo } from '/#/store'
// import type { ProjectConfig } from '/#/config';

import { createLocalStorage } from '/@/utils/cache'
import { Memory } from './memory'
import {
  TOKEN_KEY,
  ACCESS_TOKEN_KEY,
  USER_INFO_KEY,
  APP_LOCAL_CACHE_KEY,
  MENUS_KEY,
  IS_CONTINUOUS_KEY,
} from '/@/enums/cacheEnum'
import { DEFAULT_CACHE_TIME } from '/@/settings/encryptionSetting'
import { toRaw } from 'vue'

interface BasicStore {
  [TOKEN_KEY]: string | number | null | undefined
  [ACCESS_TOKEN_KEY]: string | number | null | undefined
  [MENUS_KEY]: string | number | null | undefined
  [IS_CONTINUOUS_KEY]: boolean | null | undefined
  [USER_INFO_KEY]: UserInfo
}

type LocalStore = BasicStore

export type BasicKeys = keyof BasicStore
type LocalKeys = keyof LocalStore

const ls = createLocalStorage()

const localMemory = new Memory(DEFAULT_CACHE_TIME)
const sessionMemory = new Memory(DEFAULT_CACHE_TIME)

function initPersistentMemory() {
  const localCache = ls.get(APP_LOCAL_CACHE_KEY)
  // const sessionCache = ss.get(APP_SESSION_CACHE_KEY);
  // eslint-disable-next-line no-unused-expressions
  localCache && localMemory.resetCache(localCache)
  // sessionCache && sessionMemory.resetCache(sessionCache);
}

export class Persistent {
  static getLocal<T>(key: LocalKeys) {
    return localMemory.get(key)?.value as Nullable<T>
  }

  static setLocal(
    key: LocalKeys,
    value: LocalStore[LocalKeys],
    immediate = false,
  ): void {
    localMemory.set(key, toRaw(value))
    // eslint-disable-next-line no-unused-expressions
    immediate && ls.set(APP_LOCAL_CACHE_KEY, localMemory.getCache)
  }

  static removeLocal(key: LocalKeys, immediate = false): void {
    localMemory.remove(key)
    // eslint-disable-next-line no-unused-expressions
    immediate && ls.set(APP_LOCAL_CACHE_KEY, localMemory.getCache)
  }

  static clearLocal(immediate = false): void {
    localMemory.clear()
    // eslint-disable-next-line no-unused-expressions
    immediate && ls.clear()
  }

  static clearAll(immediate = false) {
    sessionMemory.clear()
    localMemory.clear()
    if (immediate) {
      ls.clear()
      // ss.clear();
    }
  }
}

initPersistentMemory()
