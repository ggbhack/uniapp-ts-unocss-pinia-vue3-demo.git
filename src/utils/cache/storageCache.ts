import { cacheCipher } from '/@/settings/encryptionSetting'

import type { EncryptionParams } from '/@/utils/cipher'

import { AesEncryption } from '/@/utils/cipher'

import { isNullOrUnDef } from '/@/utils/is'

export interface CreateStorageParams extends EncryptionParams {
  prefixKey: string
  hasEncrypt: boolean
  timeout?: Nullable<number>
}
export const createStorage = ({
  prefixKey = '',
  key = cacheCipher.key,
  iv = cacheCipher.iv,
  timeout = null,
  hasEncrypt = true,
}: Partial<CreateStorageParams> = {}) => {
  if (hasEncrypt && [key.length, iv.length].some((item) => item !== 16)) {
    throw new Error('When hasEncrypt is true, the key or iv must be 16 bits!')
  }

  const encryption = new AesEncryption({ key, iv })

  /**
   *Cache class
   *Construction parameters can be passed into sessionStorage, localStorage,
   * @class Cache
   * @example
   */
  const WebStorage = class WebStorage {
    private prefixKey?: string

    private encryption: AesEncryption

    private hasEncrypt: boolean

    /**
     *
     * @param {*} storage
     */
    constructor() {
      this.prefixKey = prefixKey
      this.encryption = encryption
      this.hasEncrypt = hasEncrypt
    }

    // eslint-disable-next-line no-shadow
    private getKey(key: string) {
      return `${this.prefixKey}${key}`.toUpperCase()
    }

    /**
     *
     *  Set cache
     * @param {string} key
     * @param {*} value
     * @expire Expiration time in seconds
     * @memberof Cache
     */
    // eslint-disable-next-line no-shadow
    set(key: string, value: any, expire: number | null = timeout) {
      const stringData = JSON.stringify({
        value,
        time: Date.now(),
        expire: !isNullOrUnDef(expire)
          ? new Date().getTime() + expire * 1000
          : null,
      })
      const stringifyValue = this.hasEncrypt
        ? this.encryption.encryptByAES(stringData)
        : stringData
      uni.setStorageSync(this.getKey(key), stringifyValue)
    }

    /**
     *Read cache
     * @param {string} key
     * @memberof Cache
     */
    // eslint-disable-next-line no-shadow, consistent-return
    get(key: string, def: any = null): any {
      const val = uni.getStorageSync(this.getKey(key))
      if (!val) return def

      try {
        const decVal = this.hasEncrypt ? this.encryption.decryptByAES(val) : val
        const data = JSON.parse(decVal)
        const { value, expire } = data
        if (isNullOrUnDef(expire) || expire >= new Date().getTime()) {
          return value
        }
        this.remove(key)
      } catch (e) {
        return def
      }
    }

    /**
     * Delete cache based on key
     * @param {string} key
     * @memberof Cache
     */
    // eslint-disable-next-line no-shadow
    remove(key: string) {
      uni.removeStorage({ key: this.getKey(key) })
    }

    /**
     * Delete all caches of this instance
     */
    // eslint-disable-next-line class-methods-use-this
    clear(): void {
      uni.clearStorage()
    }
  }
  return new WebStorage()
}
