/***
 * 主题配置
 */

import { ThemeEnum } from '@/enums/themeEnum'

// 主品牌色 用于选中按钮或是选中文字状态，按钮，标签
export const colorPrimary = '#CE3939'
// 次主品牌色 标签，购物按钮，价格文字，警告文字
export const colorSecondary = '#fa5151'
// 辅助色 未选中状态，点缀颜色，标签，评论星级
export const colorTertiary = '#9d9d9d'

export const themeDefault: ThemeConfigModel = {
  /** 品牌色 */
  primary: '#570df8' /** 主要 */,
  secondary: '#f000b8' /** 次要 */,
  accent: '#37cdbe' /** 强调 */,
  neutral: '#3d4451' /** 中和 */,
  base: ' #7f899a' /** 基础 */,

  /** 通用色 */
  info: '#3b82f6' /** 信息 */,
  success: '#10b981' /** 成功 */,
  warning: '#f59e0b' /** 警告 */,
  error: '#ef4444' /** 错误 */,

  /** 点击时，品牌色 */
  'primary-focus': '#4406cb' /** 主要 */,
  'secondary-focus': '#bd0091' /** 次要 */,
  'accent-focus': '#2aa79b' /** 强调 */,
  'neutral-focus': '#2a2e37' /** 中和 */,
  'base-focus': '#656f7f' /** 基础 */,

  /** 点击时，通用色 */
  'info-focus': '#2563eb' /** 信息 */,
  'success-focus': '#059669' /** 成功 */,
  'warning-focus': '#d97706' /** 警告 */,
  'error-focus': '#dc2626' /** 错误 */,
  navBar: {
    backgroundColor: '#FFF',
    frontColor: '#000000',
  },
  tabBar: {
    backgroundColor: '#FFF',
    color: '#333',
    selectedColor: '#0BB640',
    borderStyle: 'white',
  },
}
const themeB: ThemeConfigModel = {
  ...themeDefault,
  primary: 'red',
  navBar: {
    backgroundColor: '#333',
    frontColor: '#ffffff',
  },
  tabBar: {
    backgroundColor: '#333',
    color: '#fff',
    selectedColor: '#0BB640',
    borderStyle: 'black',
  },
}
const themeC: ThemeConfigModel = {
  ...themeDefault,
  primary: 'yellow',
  navBar: {
    backgroundColor: '#e9e9e9',
    frontColor: '#ffffff',
  },
  tabBar: {
    backgroundColor: '#333',
    color: '#fff',
    selectedColor: '#0BB640',
    borderStyle: 'black',
  },
}
/***
 * 配置默认的主题
 */
export const themeConfig: Record<ThemeEnum, ThemeConfigModel> = {
  '': themeDefault,
  'theme-B': themeB,
  'theme-C': themeC,
}

export interface ThemeConfigModel {
  primary: string
  secondary: string
  accent: string
  neutral: string
  base: string
  info: string
  success: string
  warning: string
  error: string
  'primary-focus': string
  'secondary-focus': string
  'accent-focus': string
  'neutral-focus': string
  'base-focus': string
  'info-focus': string
  'success-focus': string
  'warning-focus': string
  'error-focus': string
  navBar: NavBarStyle
  tabBar: TabBarStyle
}

interface TabBarStyle {
  backgroundColor: string
  color: string
  selectedColor: string
  borderStyle: string
}

interface NavBarStyle {
  backgroundColor: string
  frontColor: string
}
