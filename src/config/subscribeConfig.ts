/**
 * 所有订阅消息配置
 *
 * */
import { SubscribeKeyEnum } from '@/enums/subscribeKeyEnum'
import { wxSubscribeMessage } from '@/utils/uniUtil'

export interface WxSubscriConfigModel {
  desc: string
  code: string
  types: SubscribeKeyEnum[]
}
export const wxSubscriConfig: WxSubscriConfigModel[] = [
  /** ----- 品牌商、零售商申请审核结果通知 ------*/
  {
    desc: '审核通知',
    code: '0yksDCTSguIcQmeutTsW8anCLdRXcjKzkAncKpe3_D4',
    types: [SubscribeKeyEnum.SELL_APPLY, SubscribeKeyEnum.BRAND_APPLY],
  },
  /** ----- 客户下单通知[发给品牌商] ------*/
  {
    desc: '接单通知',
    code: 'j1xjN8d4auoghR39Cz0YMSQ_ZS0tsxCOljTo5sBAYu4',
    types: [SubscribeKeyEnum.BRAND_APPLY],
  },
  /** ----- 订单已发货提醒[发给零售商] ------*/
  {
    desc: '订单发货提醒',
    code: 'G8GDh4-OACAXfvWYKoZ8vnqkM2qJzFZOizrUCtPN9iI',
    types: [SubscribeKeyEnum.SELL_ORDER],
  },
  /** ----- 订单取消通知[发给零售商] ------*/
  {
    desc: '订单取消通知',
    code: 'r42iYlkU6b3kk3gNabLHP2UvDXgutTuBlQ0hq3kpPWI',
    types: [SubscribeKeyEnum.SELL_ORDER],
  },
  /** ----- 售后申请通知[发给品牌商] ------*/
  {
    desc: '售后申请通知',
    code: 'hS1-QVGuaGXhv8-9gLlPlmt59OPqV-MOPBbgbmUMtpg',
    types: [SubscribeKeyEnum.BRAND_APPLY],
  },
  /** ----- 订单完成通知[发给零售商] ------*/
  {
    desc: '订单完成通知',
    code: '87DQrukRA6-ysyh0PTGBqpMA5F-mSEQomTXRvU29C_4',
    types: [SubscribeKeyEnum.SELL_ORDER],
  },
]
/**
 * 获取订阅消息模板ids
 * @param {String} key = SubscribeKeyEnum
 */
export const getTempIds = (key: SubscribeKeyEnum): string[] =>
  wxSubscriConfig.filter((v) => v.types.includes(key)).map((v) => v.code)
/**
 * 获取订阅消息模板信息
 * @param {String} key = SubscribeKeyEnum
 */
export const getTempConfigs = (key: SubscribeKeyEnum): WxSubscriConfigModel[] =>
  wxSubscriConfig.filter((v) => v.types.includes(key))

/**
 * @description 触发订阅消息
 * @param  {String} key = SubscribeKeyEnum
 */
export const wxSubScribe = (key: SubscribeKeyEnum) =>
  wxSubscribeMessage(getTempIds(key))
