export enum PageEnum {
  /// //////////////// pages ///////////////////////
  // basic login path
  BASE_LOGIN = '/pages/login/login',
  // basic home path
  BASE_HOME = '/pages/index/index',
  // basic detail path
  BASE_DETAIL = '/pages/detail/detail',
  // 视频页面
  BASE_VIDEO = '/pages/video/video',
  // 相册页面
  BASE_ALBUM = '/pages/album/album',
  // 下载照片
  BASE_DOWNLOAD = '/pages/download/download',
}
