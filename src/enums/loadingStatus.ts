export enum LoadingStatusEnum {
  NULL, // 没有加载
  SEARCH, // 搜索加载
  BIND, // 绑定加载
  SEND, // 发送加载
  BACK, // 归还加载
}
