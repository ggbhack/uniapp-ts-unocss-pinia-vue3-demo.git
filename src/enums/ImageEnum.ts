import EmptyImg from '/@/static/common/empty.png'

export enum ImageEnum {
  /// //////////////// common ///////////////////////
  // common 公共图片

  LOGO = '/static/common/logo.png',
  BG = '/static/common/bg.jpg',
  WX = '/static/award/wx.jpg',
  ALI = '/static/award/ali.png',
}

export { EmptyImg }
