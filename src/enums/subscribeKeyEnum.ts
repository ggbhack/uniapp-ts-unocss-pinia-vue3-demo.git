export enum SubscribeKeyEnum {
  BRAND_APPLY = 'BRAND_APPLY',
  SELL_APPLY = 'SELL_APPLY',
  SELL_ORDER = 'SELL_ORDER',
}
