// token key
export const TOKEN_KEY = 'TOKEN__'

// access_token key
export const ACCESS_TOKEN_KEY = 'ACCESS__TOKEN__'

export const LOCALE_KEY = 'LOCALE__'
// 主题
export const THEME_KEY = 'AppTheme'

// user info key
export const USER_INFO_KEY = 'USER__INFO__'

// base global local key
export const APP_LOCAL_CACHE_KEY = 'COMMON__LOCAL__KEY__'

// MENUS_KEY
export const MENUS_KEY = 'MENUS_KEY'
// CONTINUOUS_KEY
export const IS_CONTINUOUS_KEY = 'IS_CONTINUOUS_KEY'

// eslint-disable-next-line no-shadow
export enum CacheTypeEnum {
  SESSION,
  LOCAL,
}
