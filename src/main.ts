import { createSSRApp } from 'vue'
import * as Pinia from 'pinia'
import 'virtual:svg-icons-register'
// #ifdef H5
import 'webrtc-adapter'
// #endif
// @ts-ignore
import App from './App.vue'
import { setupI18n } from '/@/locales/setupI18n'
import mpShare from '@/uni_modules/uview-plus/libs/mixin/mpShare'
import themeMixin from '@/mixin/themeMixin'
// 引入 tob-less
import setupTobLess from '@/uni_modules/tob-less/index.js'

// unocss
import 'uno.css'
import { setupStore } from './store'
import { initAppConfigStore } from './logics/initAppConfig'
import { registerGlobComp } from './components/registerGlobComp'

export function createApp() {
  const app = createSSRApp(App)

  // Configure store
  setupStore(app)
  // 多主题配置
  setupTobLess(app)
  // Initialize internal system configuration
  initAppConfigStore()
  // 语言包
  setupI18n(app)
  // 注册全局组件
  registerGlobComp(app)
  // 配置分享
  app.mixin(mpShare)
  app.mixin(themeMixin)

  return {
    app,
    // uni-app 官方文档示例 https://zh.uniapp.dcloud.io/tutorial/vue3-pinia.html#%E7%8A%B6%E6%80%81%E7%AE%A1%E7%90%86-pinia
    Pinia, // 此处必须将 Pinia 返回
  }
}
