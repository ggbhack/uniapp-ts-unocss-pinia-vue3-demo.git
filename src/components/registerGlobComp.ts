import type { App } from 'vue'
// @ts-ignore
import uviewPlus from '@/uni_modules/uview-plus'
// import voiceInputButton from 'voice-input-button2'

export function registerGlobComp(app: App) {
  app.use(uviewPlus)
  // 如此配置即可
  uni.$u.config.unit = 'rpx'
  // app.use(voiceInputButton, {
  //   appId: '5e7b6060', // 您申请的语音听写服务应用的ID
  //   apiKey: 'a4c2512d39f39bd88234fa4e13eb03d3', // 您开通的语音听写服务的 apiKey
  //   apiSecret: '551155bdfb4e6007d69882ffa7594a71', // 您开通的语音听写服务的 apiSecret
  //   color: '#fff', // 按钮图标的颜色
  //   tipPosition: 'top' // 提示条位置
  // })
}
