import { AxiosResponse } from 'axios'
import { ContentTypeEnum } from '@/enums/httpEnum'
import { LoginParam } from './model/loginModel'
import { UserModel } from './model/userModal'
import { defHttp } from '/@/utils/http/axios'
import { Result } from '/#/axios'

const prefix = '/'

// api接口列表
enum Api {
  login = 'login',
}

/**
 * 手机号登录
 * @param params
 * @returns
 */
export const login = (data: LoginParam) =>
  defHttp.post<AxiosResponse<Result<UserModel>>>(
    {
      url: prefix + Api.login,
      data,
      headers: {
        'Content-Type': ContentTypeEnum.FORM_URLENCODED,
      },
    },
    { isReturnNativeResponse: true },
  )
