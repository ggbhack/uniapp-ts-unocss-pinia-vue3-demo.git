import { ResultData } from '@/api/model/baseModel'

export interface ResultFileInfoModel {
  fileInfo: FileInfoModel
}
export interface FileInfoModel {
  channel: number
  storage: string
  domain: string
  file_name: string
  file_path: string
  file_size: number
  file_ext: string
  file_type: number
  uploader_id: number
  update_time: string
  file_id: string
  preview_url: string
  external_url: string
}

export type UploadResultModel = ResultData<ResultFileInfoModel>
