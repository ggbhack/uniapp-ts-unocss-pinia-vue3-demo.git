export interface UserModel {
  id: number
  cmptId: number
  loginCode: string
  userName?: string
  lastLoginTime?: any
  createTime: string
  createUser?: any
  updateUser: string
  updateTime: string
  cmptName: string
}
