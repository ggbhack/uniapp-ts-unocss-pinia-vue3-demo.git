import { useGlobSetting } from '/@/hooks/setting'
import { defHttp } from '@/utils/http/axios'
import { SessionModel } from './model/commonModel'
/**
 * 公共模块
 */
// api接口列表
enum Api {
  upload = '/upload',
  initSession = '/initSession',
}

const { urlPrefix, apiUrl, uploadUrl } = useGlobSetting()

export const uploadFile = (formData: FormData) => {
  uni.showLoading()
  return fetch(apiUrl + urlPrefix + uploadUrl, {
    method: 'POST',
    body: formData,
  })
    .then((response) => response.json())
    .finally(() => {
      uni.hideLoading()
    })
}

/**
 * 获取session
 * @returns
 */
export const getDidSession = () =>
  defHttp.get<SessionModel>({ url: Api.initSession })
