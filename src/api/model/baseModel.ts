export interface BasicPageParams {
  page?: number
  pageSize?: number
}

/**
 * 默认的分页模型
 */
export type ResultPage<T> = {
  total: number
  pages: number
  current: number
  hitCount: boolean
  offset: number
  per_page: number
  orders?: any[]
  last_page: number
  records: T[]
  searchCount: boolean
  optimizeCountSql: boolean
  size: number
}

/**
 * 详情数据
 */
export interface ResultDetail<T> {
  message: string
  data: {
    detail: T
  }
}

/**
 * 基础data数据
 */
export interface ResultData<T = any> {
  message: string
  data: T
}

/**
 * 默认id类型
 */
export type BaseFormId = {
  id: string | number
}
/**
 * 默认表单类型
 */
export type BaseFormParams<T> = {
  form: T
  id?: string | number
}
