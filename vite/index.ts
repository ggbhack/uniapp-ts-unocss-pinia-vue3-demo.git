import AutoImport from 'unplugin-auto-import/vite'
import ViteRestart from 'vite-plugin-restart'
import Unocss from 'unocss/vite'
import UnocssIcons from '@unocss/preset-icons'
import uni from '@dcloudio/vite-plugin-uni'
import { AutoImportResolvers } from './shared/resolvers'
import { configSvgIconsPlugin } from './plugins/svgSprite'
import { isProduction } from './shared/env'

export default () => {
  return [
    uni(),
    // https://github.com/antfu/unocss
    Unocss({
      // 但 `presets` 被指定时，默认的预设将会被禁用，
      // 因此你可以在你原有的 App 上使用纯 CSS 图标而不需要担心 CSS 冲突的问题。
      presets: [
        UnocssIcons({
          // 其他选项
          prefix: 'i-',
          extraProperties: {
            display: 'inline-block',
          },
        }) as any,
        // presetUno() - 取消注释以启用默认的预设
      ],
    }),
    // // 生成 title
    // GenerateTitle(),
    // // 将包信息文件作为 vite 的配置文件之一，为 vite-plugin-optimize-persist 所用
    // PkgConfig(),
    // // 依赖预构建分析，提高大型项目性能
    // OptimizationPersist(),
    // // vue 官方插件，用来解析 sfc
    // Vue({
    // 	include: [/\.vue$/, /\.md$/]
    // }),
    // // markdown 编译插件
    // Markdown(),
    // // 文件路由
    // Pages({
    // 	extensions: ['vue', 'md', 'tsx']
    // }),
    // // 布局系统
    // Layouts(),
    // // 调试工具
    // Inspect({
    // 	enabled: env.VITE_APP_INSPECT
    // }),
    // // windicss 插件
    // Windicss({
    // 	safelist: markdownWrapperClasses
    // }),
    // // mock 服务
    // viteMockServe({
    // 	prodEnabled: env.VITE_APP_MOCK_IN_PRODUCTION
    // }),
    // // 组件自动按需引入
    // Components({
    // 	extensions: ['vue', 'md', 'tsx'],
    // 	include: [/\.md$/, /\.vue$/, /\.tsx$/],
    // 	dts: resolve(__dirname, './types/components.d.ts'),
    // 	resolvers: [
    // 		ArcoResolver(),
    // 		IconsResolver(),
    // 		NaiveUiResolver(),
    // 		ElementPlusResolver(),
    // 		AntDesignVueResolver(),
    // 		VueUseComponentsResolver()
    // 	]
    // }),
    // // 目录下 api 按需自动引入辅助插件
    // env.VITE_APP_API_AUTO_IMPORT &&
    // 	env.VITE_APP_DIR_API_AUTO_IMPORT &&
    // 	DirResolverHelper(),
    // // api 自动按需引入
    // env.VITE_APP_API_AUTO_IMPORT &&
    AutoImport({
      dts: '../types/auto-imports.d.ts',
      imports: ['vue', 'pinia', 'vue-i18n', '@vueuse/core'],
      resolvers: AutoImportResolvers,
    }),
    // // i18n 国际化支持
    // I18n({
    // 	runtimeOnly: true,
    // 	compositionOnly: true,
    // 	include: [resolve(__dirname, '../locales/**')]
    // }),
    // // 预设热重启服务
    ViteRestart({
      restart: ['.env*', 'presets/tov.[jt]s', 'presets/shared/**/*'],
    }),
    // // tsx 支持
    // vueJsx()
    // // 生产环境资源压缩
    // viteCompression({
    // 	// @ts-ignore
    // 	algorithm: env.VITE_APP_COMPRESSINON_ALGORITHM
    // }),
    // // 对 vite-plugin-vue-layouts 的 hmr 问题的临时处理
    // // 如果 https://github.com/JohnCampionJr/vite-plugin-vue-layouts/pull/58 被接受的话，未来可能会移除
    // FixLayoutsHmr()
    configSvgIconsPlugin(isProduction),
  ]
}
